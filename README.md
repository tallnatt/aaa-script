# aaa-script

AAA Validations Automation Script

## Description
This script allows for automating the AAA validations which are asked of WCO to perform before and after certain changes and maintance activies.


## Installation
No installtion needed, but permissions and ownership of files need to be changed by running "sudo ~/aaa-script/chperm.sh"

## Usage
From your home folder execute "./aaa-validations.sh"
