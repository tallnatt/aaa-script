#!/usr/bin/bash

user=$(logname)
debug=0
filename="/home/$user/aaa-validations-$(date +%Y%m%d%H%M%S).txt"
email=$(sudo -iu $user git config user.email)
PIDS=""

sudo -iu alu touch $filename

if [ $debug -eq 1 ]
then
        echo ""
        echo "======================================================"
        echo "=========DEBUG MODE - NO VALIDATIONS PERFORMED========"
        echo "=================CONTACT SCRIPT ADMIN================="
        echo "======================================================"
        echo "Logged in User: $user"
	echo "Effectual User: $(sudo -iu alu whoami)"
        echo "Filename: $filename"
	echo "Email: $email"
        echo "======================================================"
	echo ""
        stat $filename
        rm -f $filename
        echo ""
elif [ $debug -eq 0 ]
then
        echo "======NCDE Validations======" > $filename
        echo "nc -w1 -vz 98.8.143.66 389" >> $filename
        script -c 'nc -w1 -vz 98.8.143.66 389' | grep Connect >> $filename
        echo "nc -w1 -vz 98.8.143.66 636" >> $filename
        script -c 'nc -w1 -vz 98.8.143.66 636' | grep Connect >> $filename

        echo "" >> $filename
        echo "======NCDW Validations======" >> $filename
        echo "nc -w1 -vz 98.8.191.193 389" >> $filename
        script -c 'nc -w1 -vz 98.8.191.193 389' | grep Connect >> $filename
        echo "nc -w1 -vz 98.8.191.193 636" >> $filename
        script -c 'nc -w1 -vz 98.8.191.193 636' | grep Connect >> $filename

        #======Acct Validations to VIPs======
        sudo -iu alu /opt/wno_tools/scripts/AAA/validate_aaa/acct_validation-vips.sh > /home/$user/acct_validation-vips.txt &
        PIDS+="$! "
        sudo -iu alu /opt/wno_tools/scripts/AAA/validate_aaa/acct_validation-vips-bhn.sh  > /home/$user/acct_validation-vips-bhn.txt &
        PIDS+="$! "

        #======Acct Validations to Nodes======
        sudo -iu alu /opt/wno_tools/scripts/AAA/validate_aaa/acct_validation-nodes.sh  > /home/$user/acct_validation-nodes.txt &
        PIDS+="$! "
        sudo -iu alu /opt/wno_tools/scripts/AAA/validate_aaa/acct_validation-nodes-bhn.sh > /home/$user/acct_validation-nodes-bhn.txt &
        PIDS+="$! "


        #======Auth Validations to VIPs======
        sudo -iu alu /opt/wno_tools/scripts/AAA/validate_aaa/auth_validation-vip.sh > /home/$user/auth_validation-vip.txt &
        PIDS+="$! "
        sudo -iu alu /opt/wno_tools/scripts/AAA/validate_aaa/auth_validation-vip-bhn.sh > /home/$user/auth_validation-vip-bhn.txt &
        PIDS+="$! "

        #======Auth Validations to Nodes======
        sudo -iu alu /opt/wno_tools/scripts/AAA/validate_aaa/auth_validation-nodes.sh > /home/$user/auth_validation-nodes.txt &
        PIDS+="$! "
        sudo -iu alu /opt/wno_tools/scripts/AAA/validate_aaa/auth_validation-nodes-bhn.sh > /home/$user/auth_validation-nodes-bhn.txt &
        PIDS+="$! "

        wait $PIDS
        echo "" >> $filename
        echo "======Acct Validations to VIPs======" >> $filename
        cat /home/$user/acct_validation-vips.txt >> $filename
        cat /home/$user/acct_validation-vips-bhn.txt >> $filename
        echo "" >> $filename
        echo "======Acct Validations to Nodes======"  >> $filename
        cat /home/$user/acct_validation-nodes.txt >> $filename
        cat /home/$user/acct_validation-nodes-bhn.txt >> $filename
        echo "" >> $filename
        echo "======Auth Validations to VIPs======" >> $filename
        cat /home/$user/auth_validation-vip.txt >> $filename
        cat /home/$user/auth_validation-vip-bhn.txt >> $filename
        echo "" >> $filename
        echo "======Auth Validations to Nodes======" >> $filename
        cat /home/$user/auth_validation-nodes.txt >> $filename
        cat /home/$user/auth_validation-nodes-bhn.txt >> $filename

        rm /home/$user/acct_validation-vips.txt
        rm /home/$user/acct_validation-vips-bhn.txt
        rm /home/$user/acct_validation-nodes.txt
        rm /home/$user/acct_validation-nodes-bhn.txt
        rm /home/$user/auth_validation-vip.txt
        rm /home/$user/auth_validation-vip-bhn.txt
        rm /home/$user/auth_validation-nodes.txt
        rm /home/$user/auth_validation-nodes-bhn.txt

        count=$(cat $filename | grep -qE '[[:alpha:]]+-[[:alpha:]]+[[:blank:]]+0' | wc -l)
        if [ $count -eq 0 ]
        then
                echo "Validations Completed: Success - No Failures." | mailx -a $filename -s "Validations $(date +%Y%m%d%H%M%S)" $email
        else
                echo "Validations Completed: $count Failures - Check Results File for Responses with 0." | mailx -a $filename -s "Validations $(date +%Y%m%d%H%M%S)" $email
        fi
fi
