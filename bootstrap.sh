#!/usr/bin/bash

if [ "$EUID" -ne 0 ]
then
        echo "Checking for Updates."
        cd aaa-script
        git pull
        echo "Checking Permissions"
        sudo ./chperm.sh
        echo "Running Validations"
        cd ..
        sudo aaa-script/waitspin.sh aaa-script/aaa-validations.sh
else
        echo "Do not run using sudo!"
fi
