#!/usr/bin/bash
user=$(logname)
groups | grep -q wheel

if [ $? -eq 1 ]
then
        echo "You need to be part of the wheel group to run this script."
        exit
fi

if [ "$EUID" -ne 0 ]
then
        echo "Please run this script again using the sudo command."
        exit
fi

if [ $(stat -c %G /home/$user) != "alu" ] || [ $(stat -c %a /home/$user) -ne 770 ] || [ $(stat -c %G /home/$user/aaa-script) != "alu" ] || [ $(stat -c %a /home/$user/aaa-script) -ne 770 ] || [ $(stat -c %G /home/$user/aaa-script/aaa-validations.sh) != "alu" ] || [ $(stat -c %a /home/$user/aaa-script/aaa-validations.sh) -ne 770 ]
then
        echo "Fixing Permissions"
        chown $user:alu /home/$user
        chmod 770 /home/$user

        chown $user:alu /home/$user/aaa-script
        chmod 770 /home/$user/aaa-script

        chown $user:alu /home/$user/aaa-script/aaa-validations.sh
        chmod 770 /home/$user/aaa-script/aaa-validations.sh

        echo "Permissions Changed Succesfully!"
        echo "run \"sudo ~/aaa-validations.sh\" to execute the validations"
fi

cp /home/$user/aaa-script/bootstrap.sh /home/$user/aaa-validations.sh
chown $user:$user /home/$user/aaa-validations.sh
